//tslint:disable
import { browser, by, element } from "protractor";

export class AngularSudokuPage {
  public navigateTo() {
    return browser.get("/");
  }

  public getParagraphText() {
    return element(by.css("sudoku-root h1")).getText();
  }
}
