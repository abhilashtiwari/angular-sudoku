import { generateOneToNine, OneToNine, OneToNineOrUndefined } from "app/service/digit";
import { Field } from "app/service/field";
import { allPositions, allPositionsInAllBoxes, allPositionsInAllColumns, allPositionsInAllRows, allPositionsInBox, allPositionsInColumn, allPositionsInRow, PosIter, PosIterIter, Position } from "app/service/position";
import { SolvedField } from "app/service/solved-field";

export type Reason =
  "OnlyPossibleDigitForCell" |
  "OnlyPossiblePositionForRow" |
  "OnlyPossiblePositionForColumn" |
  "OnlyPossiblePositionForBox";

export class Sudoku {
  private readonly grid: Field[][] = [[], [], [], [], [], [], [], [], [], []];

  constructor(initialDigits: OneToNineOrUndefined[][]) {
    for (const pos of allPositions())
      this.grid[pos.row][pos.col] = new Field();
    for (const pos of allPositions()) {
      const digit = initialDigits[pos.row][pos.col];
      if (digit !== undefined)
        this.setDigit(pos, digit);
    }
  }

  solveAll(): number {
    let solvedFieldCount = 0;
    while (this.solveNext() !== undefined)
      solvedFieldCount++;
    return solvedFieldCount;
  }

  solveNext(): SolvedField | undefined {
    try {
      this.solveOnlyPossibleDigit();
      this.solveOnlyPossiblePosition();
    } catch (e) {
      if (e instanceof SolvedField)
        return e;
      throw e;
    }
    return undefined;
  }

  private solveOnlyPossibleDigit(): void {
    for (const pos of allPositions())
      this.solveOnlyPossibleDigitForPosition(pos);
  }

  private solveOnlyPossibleDigitForPosition(pos: Position): void {
    const field = this.getField(pos);
    const onlyPossibleDigit = field.getOnlyPossibleDigit();
    if (onlyPossibleDigit !== undefined)
      this.setDigit(pos, onlyPossibleDigit, "OnlyPossibleDigitForCell");
  }

  solveOnlyPossiblePosition(): void {
    for (const digit of generateOneToNine()) {
      this.solveOnlyPossiblePositionForGroups(allPositionsInAllRows(), digit, "OnlyPossiblePositionForRow");
      this.solveOnlyPossiblePositionForGroups(allPositionsInAllColumns(), digit, "OnlyPossiblePositionForColumn");
      this.solveOnlyPossiblePositionForGroups(allPositionsInAllBoxes(), digit, "OnlyPossiblePositionForBox");
    }
  }

  private solveOnlyPossiblePositionForGroups(
    allPositionsInAllGroups: PosIterIter,
    digit: OneToNine,
    reason: Reason,
  ): void {
    for (const allPositionsInGroup of allPositionsInAllGroups)
      this.solveOnlyPossiblePositionForGroup(allPositionsInGroup, digit, reason);
  }

  private solveOnlyPossiblePositionForGroup(allPositionsInGroup: PosIter, digit: OneToNine, reason: Reason): void {
    const pos = this.getOnlyPossiblePosition(allPositionsInGroup, digit);
    if (pos !== undefined)
      this.setDigit(pos, digit, reason);
  }

  private getOnlyPossiblePosition(allPositionsInGroup: PosIter, digit: OneToNine): Position | undefined {
    let result;
    for (const pos of allPositionsInGroup)
      if (this
        .getField(pos)
        .isDigitPossible(digit))
        if (result === undefined)
          result = pos;
        else
          return undefined;
    return result;
  }

  private setDigit(pos: Position, digit: OneToNine, reason?: Reason): void {
    this.getField(pos).digit = digit;

    for (const p of allPositionsInColumn(pos))
      this.crossOutDigit(p, digit);

    for (const p of allPositionsInRow(pos))
      this.crossOutDigit(p, digit);

    for (const p of allPositionsInBox(pos))
      this.crossOutDigit(p, digit);

    if (reason !== undefined)
      throw new SolvedField(pos, digit, reason);
  }

  private crossOutDigit(pos: Position, digit: OneToNine): void {
    this
      .getField(pos)
      .crossOutDigit(digit);
  }

  getField(pos: Position): Field {
    return this.grid[pos.row][pos.col];
  }
}
