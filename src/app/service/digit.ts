export type OneFourSeven = 1 | 4 | 7;
export type OneToNine = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
export type OneToNineOrUndefined = OneToNine | undefined;
export type OneToNineOrNull = OneToNine | null;

export function* generateOneToNine(): IterableIterator<OneToNine> {
  for (let i = 1; i <= 9; i++)
    yield i as OneToNine;
}

export function* generateOneFourSeven(): IterableIterator<OneFourSeven> {
  yield 1;
  yield 4;
  yield 7;
}

export function nextLowerOfOneFourSeven(oneToNine: OneToNine): OneFourSeven {
  return (Math.floor((oneToNine - 1) / 3) * 3 + 1) as OneFourSeven;
}

export function add2ToOneFourSeven(oneFourSeven: OneFourSeven): OneToNine {
  return (oneFourSeven + 2) as OneToNine;
}
