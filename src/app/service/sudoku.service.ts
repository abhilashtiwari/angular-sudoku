import { Injectable } from "@angular/core";
import { demoGames, getLevelFactor, Level, levels, solvedDemoGames } from "app/service/demo.games";
import { generateOneToNine, OneToNine, OneToNineOrNull, OneToNineOrUndefined } from "app/service/digit";
import { allPositions, allPositionsInRegion, PosIter, Position, Region } from "app/service/position";
import { FieldState, SetDigitMessage, SetDigitResult, SetDigitResultType } from "app/service/set-digit-result";
import { SolvedField } from "app/service/solved-field";
import { Sudoku } from "app/service/sudoku";
import { assertNever } from "app/util";

export function copyAndUnshift(src: OneToNineOrUndefined[][]): OneToNineOrUndefined[][] {
  const result: OneToNineOrUndefined[][] = [];
  for (let i = 0; i < src.length; i++) {
    result[i + 1] = src[i].slice();
    result[i + 1].unshift(undefined);
  }
  return result;
}

function nullToUndefined<T>(value: T | null): T | undefined {
  return value === null ? undefined : value;
}

function undefinedToNull<T>(value: T | undefined): T | null {
  // tslint:disable-next-line:no-null-keyword
  return value === undefined ? null : value;
}

export interface GameData {
  digits?: OneToNineOrNull[];
  gameIndex?: number;
  score?: number;
}

const WARNING_SCORE_DECREMENT = 9;

@Injectable()
export class SudokuService {

  private gameIndex = -1;
  grid: OneToNineOrUndefined[][] = [[], [], [], [], [], [], [], [], [], []];
  score = 0;
  private startTime = 0;
  pointsToWin = 1;

  constructor() {
    for (const pos of allPositions())
      this.setDigit(pos);
    setInterval(() => {
      const elapsedSeconds = Math.floor((new Date().getTime() - this.startTime) / 1000);
      this.pointsToWin = Math.max(1, this.getMaxPointsToWin() - elapsedSeconds);
    }, 100);
  }

  private getMaxPointsToWin(): number {
    return WARNING_SCORE_DECREMENT * getLevelFactor(this.getLevel());
  }

  isStarted(): boolean {
    for (const pos of allPositions())
      if (this.getDigit(pos) !== undefined && !this.isInitialClue(pos))
        return true;
    return false;
  }

  getLevel(): Level {
    return levels[this.gameIndex];
  }

  isUserDefined(): boolean {
    return this.gameIndex === -1;
  }

  isSolved(): boolean {
    for (const pos of allPositions())
      if (this.getDigit(pos) === undefined)
        return false;
    return true;
  }

  newGame(): Position {
    this.startTimer();
    this.gameIndex = (this.gameIndex + 1) % demoGames.length;
    const demoGame = demoGames[this.gameIndex];
    this.grid = copyAndUnshift(demoGame);

    for (const pos of allPositions())
      if (!this.isInitialClue(pos))
        return new Position(pos.row, pos.col);
    throw new Error("no free position, all fields are initial clues!");
  }

  clearGame(): void {
    this.gameIndex = -1;
    for (const pos of allPositions())
      this.setDigit(pos);
  }

  solveNextAndUpdate(): SolvedField | undefined {
    this.startTimer();
    const s = new Sudoku(this.grid);
    const solvedField = s.solveNext();
    if (solvedField !== undefined) {
      this.setDigit(solvedField.position, solvedField.digit);
      if (!this.isUserDefined())
        this.score -= WARNING_SCORE_DECREMENT;
    }
    return solvedField;
  }

  solveAllAndUpdate(): void {
    const s = new Sudoku(this.grid);
    const solvedFieldCount = s.solveAll();
    for (const pos of allPositions())
      this.setDigit(pos, s.getField(pos).digit);
    if (!this.isUserDefined())
      this.score -= Math.max(solvedFieldCount, WARNING_SCORE_DECREMENT);
  }

  private startTimer(): void {
    this.startTime = new Date().getTime();
  }

  getDigit(position: Position): OneToNineOrUndefined {
    return this.grid[position.row][position.col];
  }

  setDigitChecked(position: Position, digit?: OneToNine): SetDigitResult | undefined {
    if (digit === undefined) {
      this.setDigit(position);
      return undefined;
    }

    const result = this.setDefinedDigitChecked(position, digit);
    if (!this.isUserDefined())
      this.score += this.getScoreIncrement(result.type);
    this.startTimer();
    return result;
  }

  private getScoreIncrement(type: SetDigitResultType): number {
    switch (type) {
      case undefined:
        return this.pointsToWin;
      case "warning":
        return -WARNING_SCORE_DECREMENT;
      case "solved":
        return 2 * this.pointsToWin;
      default:
        return assertNever(type);
    }
  }

  private setDefinedDigitChecked(position: Position, digit: OneToNine): SetDigitResult {
    const oldDigit = this.getDigit(position);
    this.setDigit(position);

    const fieldStates: FieldState[][] = [[], [], [], [], [], [], [], [], [], []];

    if (!this.isDigitPossible(position, digit)) {
      this.setDigit(position, oldDigit);
      fieldStates[position.row][position.col] = "warning";
      return new SetDigitResult("warning", "digitNotPossible", fieldStates);
    }

    if (!this.isDigitCorrect(position, digit)) {
      this.setDigit(position, oldDigit);
      fieldStates[position.row][position.col] = "warning";
      return new SetDigitResult("warning", "digitNotCorrect", fieldStates);
    }

    this.setDigit(position, digit);
    if (this.isSolved()) {
      for (const pos of allPositions())
        fieldStates[pos.row][pos.col] = "solvedSudoku";
      fieldStates[position.row][position.col] = "solvedFieldInSolvedSudoku";
      return new SetDigitResult("solved", "solved", fieldStates);
    }

    let type: SetDigitResultType;
    let message: SetDigitMessage;
    if (this.getExhaustedDigits()[digit]) {
      for (const pos of allPositions())
        if (this.getDigit(pos) === digit)
          fieldStates[pos.row][pos.col] = "solvedGroup";
      fieldStates[position.row][position.col] = "solvedFieldInSolvedGroup";
      type = "solved";
      message = "solvedDigit";
    }

    this.setFieldStatesForSolvedRegion(position, "Column", fieldStates);
    this.setFieldStatesForSolvedRegion(position, "Row", fieldStates);
    this.setFieldStatesForSolvedRegion(position, "Box", fieldStates);

    if (fieldStates[position.row][position.col] === undefined)
      fieldStates[position.row][position.col] = "solvedField";

    return new SetDigitResult(type, message, fieldStates);
  }

  private setFieldStatesForSolvedRegion(position: Position, region: Region, fieldStates: FieldState[][]): void {
    if (this.isRegionSolved(allPositionsInRegion(position, region))) {
      for (const pos of allPositionsInRegion(position, region))
        fieldStates[pos.row][pos.col] = "solvedGroup";
      fieldStates[position.row][position.col] = "solvedFieldInSolvedGroup";
    }
  }

  private isRegionSolved(posIter: PosIter): boolean {
    for (const p of posIter)
      if (this.getDigit(p) === undefined)
        return false;
    return true;
  }

  private setDigit(position: Position, digit?: OneToNine): void {
    this.grid[position.row][position.col] = digit;
  }

  getExhaustedDigits(): boolean[] {
    const occurrences: number[] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (const pos of allPositions()) {
      const digit = this.getDigit(pos);
      if (digit !== undefined)
        occurrences[digit]++;
    }
    const result: boolean[] = [];
    for (const i of generateOneToNine())
      result[i] = occurrences[i] === 9;
    return result;
  }

  getGridForOnlyOnePossibleDigit(): boolean[][] {
    const s = new Sudoku(this.grid);
    const result: boolean[][] = [[], [], [], [], [], [], [], [], [], []];
    for (const pos of allPositions())
      result[pos.row][pos.col] = s
        .getField(pos)
        .getOnlyPossibleDigit() !== undefined;
    return result;
  }

  isDigitPossible(position: Position, digit: OneToNine): boolean {
    const s = new Sudoku(this.grid);
    return s
      .getField(position)
      .isDigitPossible(digit);
  }

  isDigitCorrect(position: Position, digit: OneToNine): boolean {
    return this.isUserDefined() || solvedDemoGames[this.gameIndex][position.row - 1][position.col - 1] === digit;
  }

  isInitialClue(position: Position): boolean {
    return !this.isUserDefined() && demoGames[this.gameIndex][position.row - 1][position.col - 1] !== undefined;
  }

  setGameData(gameData: GameData): void {
    if (gameData.gameIndex !== undefined && this.gameIndex !== gameData.gameIndex)
      this.setGameIndex(gameData.gameIndex);

    if (gameData.digits !== undefined)
      this.setDigits(gameData.digits);

    if (gameData.score !== undefined)
      this.score = gameData.score;
  }

  setGameIndex(gameIndex: number): void {
    this.gameIndex = gameIndex;
    if (this.isUserDefined())
      for (const pos of allPositions())
        this.setDigit(pos);
    else
      this.grid = copyAndUnshift(demoGames[this.gameIndex]);
  }

  setDigits(digits: OneToNineOrNull[]): void {
    let i = 0;
    for (const pos of allPositions())
      if (!this.isInitialClue(pos)) {
        const digit = digits[i++];
        // only set a digit if it is defined (i.e. never remove a valid digit in a multi-player race condition)
        // or if it is a user defined game
        if (digit !== null || this.isUserDefined())
          this.setDigit(pos, nullToUndefined(digit));
      }
  }

  getGameData(): GameData {
    return {
      digits: this.getGridValuesAsArray(),
      gameIndex: this.gameIndex,
      score: this.score,
    };
  }

  private getGridValuesAsArray(): OneToNineOrNull[] {
    let i = 0;
    const result: OneToNineOrNull[] = [];
    for (const pos of allPositions())
      if (!this.isInitialClue(pos))
        result[i++] = undefinedToNull(this.getDigit(new Position(pos.row, pos.col)));
    return result;
  }
}
