import { add2ToOneFourSeven, generateOneFourSeven, generateOneToNine, nextLowerOfOneFourSeven, OneToNine } from "app/service/digit";
import { assertNever } from "app/util";

export class Position {
  constructor(public row: OneToNine, public col: OneToNine) {
  }

  nextRowBox(): void {
    this.row = incrementBox(this.row);
  }

  prevRowBox(): void {
    this.row = decrementBox(this.row);
  }

  nextColBox(): void {
    this.col = incrementBox(this.col);
  }

  prevColBox(): void {
    this.col = decrementBox(this.col);
  }

  nextRow(): void {
    this.row = increment(this.row);
  }

  prevRow(): void {
    this.row = decrement(this.row);
  }

  nextCol(): void {
    this.col = increment(this.col);
  }

  prevCol(): void {
    this.col = decrement(this.col);
  }

  next(): void {
    if (this.col < 9)
      this.col++;
    else if (this.row < 9) {
      this.row++;
      this.col = 1;
    } else
      this.start();
  }

  prev(): void {
    if (this.col > 1)
      this.col--;
    else if (this.row > 1) {
      this.row--;
      this.col = 9;
    } else
      this.end();
  }

  start(): void {
    this.row = 1;
    this.col = 1;
  }

  end(): void {
    this.row = 9;
    this.col = 9;
  }

}

function increment(i: OneToNine): OneToNine {
  if (i > 8)
    return 1;
  return (i + 1) as OneToNine;
}

function decrement(i: OneToNine): OneToNine {
  if (i < 2)
    return 9;
  return (i - 1) as OneToNine;
}

function incrementBox(i: OneToNine): OneToNine {
  if (i > 6)
    return 1;
  if (i > 3)
    return 7;
  return 4;
}

function decrementBox(i: OneToNine): OneToNine {
  if (i < 4)
    return 9;
  if (i < 7)
    return 3;
  return 6;
}

export type Region = "Row" | "Column" | "Box";

export function* allPositionsInRegion(position: Position, region: Region): PosIter {
  switch (region) {
    case "Row":
      yield* allPositionsInRow(position);
      break;
    case "Column":
      yield* allPositionsInColumn(position);
      break;
    case "Box":
      yield* allPositionsInBox(position);
      break;
    default:
      return assertNever(region);
  }
}

export type PosIter = IterableIterator<Position>;

export type PosIterIter = IterableIterator<PosIter>;

export function* allPositionsInAllRows(): PosIterIter {
  for (const row of generateOneToNine())
    yield allPositionsInRow(new Position(row, 1));
}

export function* allPositionsInAllColumns(): PosIterIter {
  for (const col of generateOneToNine())
    yield allPositionsInColumn(new Position(1, col));
}

export function* allPositionsInAllBoxes(): PosIterIter {
  for (const row of generateOneFourSeven())
    for (const col of generateOneFourSeven())
      yield allPositionsInBox(new Position(row, col));
}

export function* allPositions(): PosIter {
  yield* allPositionsBetween(1, 1, 9, 9);
}

export function* allPositionsInRow(pos: Position): PosIter {
  yield* allPositionsBetween(pos.row, 1, pos.row, 9);
}

export function* allPositionsInColumn(pos: Position): PosIter {
  yield* allPositionsBetween(1, pos.col, 9, pos.col);
}

export function* allPositionsInBox(pos: Position): PosIter {
  const row = nextLowerOfOneFourSeven(pos.row);
  const col = nextLowerOfOneFourSeven(pos.col);
  yield* allPositionsBetween(row, col, add2ToOneFourSeven(row), add2ToOneFourSeven(col));
}

function* allPositionsBetween(startRow: OneToNine, startCol: OneToNine, endRow: OneToNine, endCol: OneToNine): PosIter {
  for (let row = startRow; row <= endRow; row++)
    for (let col = startCol; col <= endCol; col++)
      yield new Position(row, col);
}
