import { inject, TestBed } from "@angular/core/testing";
import { demoGames, solvedDemoGames } from "app/service/demo.games";
import { Position } from "app/service/position";
import { SolvedField } from "app/service/solved-field";
import { copyAndUnshift, SudokuService } from "app/service/sudoku.service";

/*
TODO
Services als isolated tests (https://angular.io/guide/testing#isolated-unit-tests)
Für Angular-Tests evtl. 2 beforeEach für async und sync
Für Komponententests Stub-Services mit useValue und Object Literal verwenden
(aber nicht im Test verändern oder verwenden da Clone; TestBed.get() verwenden) oder spyOn

https://angular.io/guide/testing
*/
describe("SudokuService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SudokuService],
    });
  });

  it("should be created", inject([SudokuService], (service: SudokuService) => {
    expect(service)
      .toBeTruthy();
  }));

  it("should solve next field and update demo game 1", inject([SudokuService], (service: SudokuService) => {
    service.newGame();
    expect(service.solveNextAndUpdate())
      .toEqual(new SolvedField(new Position(7, 6), 4, "OnlyPossibleDigitForCell"));
    const expected = copyAndUnshift(demoGames[0]);
    expected[7][6] = 4;
    expect(service.grid)
      .toEqual(expected);
  }));

  it("should solve all and update all demo games", inject([SudokuService], (service: SudokuService) => {
    for (const solvedDemoGame of solvedDemoGames) {
      service.newGame();
      service.solveAllAndUpdate();
      expect(service.grid)
        .toEqual(copyAndUnshift(solvedDemoGame));
    }
  }));

  it("should return correct answer, if digit is possible", inject([SudokuService], (service: SudokuService) => {
    service.newGame();
    expect(service.isDigitPossible(new Position(1, 1), 1))
      .toBeTruthy();
    expect(service.isDigitPossible(new Position(9, 9), 1))
      .toBeFalsy();
  }));

  it("should return correct answer, if digit is correct", inject([SudokuService], (service: SudokuService) => {
    service.newGame();
    expect(service.isDigitCorrect(new Position(1, 1), 5))
      .toBeTruthy();
    expect(service.isDigitCorrect(new Position(1, 1), 1))
      .toBeFalsy();
    service.clearGame();
    expect(service.isDigitCorrect(new Position(1, 1), 1))
      .toBeTruthy();
  }));

});
