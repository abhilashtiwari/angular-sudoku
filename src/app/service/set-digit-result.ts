export type SetDigitResultType = undefined | "warning" | "solved";

export type SetDigitMessage = undefined | "digitNotCorrect" | "digitNotPossible" | "solved" | "solvedDigit";

export type FieldState = undefined | "warning" |
  "solvedField" | "solvedFieldInSolvedGroup" | "solvedGroup" | "solvedFieldInSolvedSudoku" | "solvedSudoku";

export class SetDigitResult {
  constructor(
    readonly type: SetDigitResultType,
    readonly message: SetDigitMessage,
    readonly fieldStates: FieldState[][],
  ) { }
}
