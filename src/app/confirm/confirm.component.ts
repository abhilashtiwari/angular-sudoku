import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from "@angular/material";

interface Data {
  title: string;
  question: string;
}

@Component({
  styleUrls: ["./confirm.component.scss"],
  templateUrl: "./confirm.component.html",
})
export class ConfirmComponent {
  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) readonly data: Data,
  ) { }
}

export function confirm(callHandlerWithoutDialog: boolean, dialog: MatDialog, title: string,
  question: string, confirmHandler: () => void): void {
  if (callHandlerWithoutDialog)
    confirmHandler();
  else {
    const dialogRef = dialog.open<ConfirmComponent, Data>(ConfirmComponent, { data: { question, title } });

    dialogRef
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result)
          confirmHandler();
      });
  }
}
