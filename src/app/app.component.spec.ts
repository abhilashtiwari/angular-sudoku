/*
temporarily disabled because auf firebase
import { TestBed, async } from '@angular/core/testing';

import { HttpModule } from '@angular/http';
import {
  TranslateModule, TranslateLoader, TranslateFakeLoader,
  MissingTranslationHandler, MissingTranslationHandlerParams
} from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import {
  MatButtonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatIconModule,
  MatDialogModule,
  MatSnackBarModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    return params.key + (params.interpolateParams ? ": " + JSON.stringify(params.interpolateParams) : "");
  }
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule, HttpModule,
        MatButtonModule,
        MatSidenavModule,
        MatToolbarModule,
        MatDialogModule,
        MatIconModule,
        MatSnackBarModule,
        TranslateModule.forRoot({
          missingTranslationHandler: { provide: MissingTranslationHandler, useClass: MyMissingTranslationHandler },
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader
          }
        }),
        AngularFireModule.initializeApp({
          apiKey: "AIzaSyDLtCpl-B0yD4_Nr-ulcokswM9PKnK05IM",
          authDomain: "angular-sudoku.firebaseapp.com",
          databaseURL: "https://angular-sudoku.firebaseio.com",
          projectId: "angular-sudoku",
          storageBucket: "angular-sudoku.appspot.com",
          messagingSenderId: "995445029311"
        }),
        AngularFirestoreModule,
        AngularFireAuthModule
      ],
      declarations: [
        AppComponent
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Sudoku');
  }));

  it(`should have empty reason`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.reason).toEqual('');
  }));

  it(`should have defined reason after solve next`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    app.solveNext();
    expect(app.reason)
    .toEqual('OnlyPossibleDigitForCell: {"position":{"row":7,"col":6},"digit":4,"reason":"OnlyPossibleDigitForCell"}');
  }));
});
*/
