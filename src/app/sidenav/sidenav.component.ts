import { Component } from "@angular/core";
import { AngularFireAuth } from "angularfire2/auth";

export interface Theme {
  name: string;
  primaryColor: string;
  accentColor: string;
  backgroundColor: string;
}

export abstract class SidenavApp {
  abstract readonly currentTheme: string;
  abstract readonly themes: Theme[];
  abstract readonly auth: AngularFireAuth;

  abstract newGame(): void;
  abstract ownGame(): void;

  abstract solveNext(): void;
  abstract solveAll(): void;
  abstract isSolved(): boolean;

  abstract setTheme(theme: string): void;

  abstract login(): void;
  abstract logout(): void;
  abstract about(): void;
}

@Component({
  selector: "sudoku-sidenav",
  styleUrls: ["./sidenav.component.scss"],
  templateUrl: "./sidenav.component.html",
})
export class SidenavComponent {

  constructor(public app: SidenavApp) {
  }
}
