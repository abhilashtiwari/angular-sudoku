import { OverlayContainer } from "@angular/cdk/overlay";
import { Component, ElementRef, forwardRef, Provider, Renderer2, ViewChild } from "@angular/core";
import { MatDialog, MatSidenav, MatSnackBar, MatSnackBarConfig } from "@angular/material";
import { TranslateService } from "@ngx-translate/core";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFirestore, AngularFirestoreDocument } from "angularfire2/firestore";
import { confirm } from "app/confirm/confirm.component";
import { DigitApp, DigitCssClass } from "app/digit/digit.component";
import { FieldCssClass, GridApp } from "app/grid/grid.component";
import { LoginComponent } from "app/login/login.component";
import { Level } from "app/service/demo.games";
import { generateOneToNine, OneToNine, OneToNineOrUndefined } from "app/service/digit";
import { allPositions, Position } from "app/service/position";
import { FieldState } from "app/service/set-digit-result";
import { SolvedField } from "app/service/solved-field";
import { GameData, SudokuService } from "app/service/sudoku.service";
import { SidenavApp, Theme } from "app/sidenav/sidenav.component";
import { ToolbarApp } from "app/toolbar/toolbar.component";
import { filter, map, switchMap, tap } from "rxjs/operators";

export interface UserData {
  theme?: string;
  gameData?: GameData;
}

@Component({
  providers: [
    SudokuService,
    provideAppComponentAs(SidenavApp),
    provideAppComponentAs(ToolbarApp),
    provideAppComponentAs(GridApp),
    provideAppComponentAs(DigitApp),
  ],
  selector: "sudoku-root",
  styleUrls: ["./app.component.scss"],
  templateUrl: "./app.component.html",
})
export class AppComponent implements SidenavApp, ToolbarApp, GridApp, DigitApp {
  private userDoc?: AngularFirestoreDocument<UserData>;
  private selectedPosition: Position;
  private selectedPositionVisible = false;
  private selectedDigit?: OneToNine = 1;
  private lastSolvedField?: SolvedField;

  @ViewChild(MatSidenav)
  readonly sidenav!: MatSidenav;

  currentTheme = "";
  reason = "";
  fieldCssClasses: FieldCssClass[][] = [];
  digitCssClasses: DigitCssClass[] = [];
  fieldStates: FieldState[][] = [];
  themes: Theme[] = [
    {
      accentColor: "#e91e63",
      backgroundColor: "#fafafa",
      name: "",
      primaryColor: "#3f51b5",
    },
    {
      accentColor: "#ffc107",
      backgroundColor: "#fafafa",
      name: "light-deeppurple-amber",
      primaryColor: "#673ab7",
    },
    {
      accentColor: "#4caf50",
      backgroundColor: "#303030",
      name: "dark-purple-green",
      primaryColor: "#9c27b0",
    },
    {
      accentColor: "#607d8b",
      backgroundColor: "#303030",
      name: "dark-pink-bluegrey",
      primaryColor: "#e91e63",
    },
  ];

  // tslint:disable-next-line
  constructor(
    private readonly ts: TranslateService,
    private readonly db: AngularFirestore,
    private readonly ss: SudokuService,
    private readonly snackBar: MatSnackBar,
    private readonly element: ElementRef,
    private readonly renderer: Renderer2,
    private readonly overlayContainer: OverlayContainer,
    private readonly dialog: MatDialog,
    readonly auth: AngularFireAuth,
  ) {
    // Workaround until fix for https://github.com/angular/angularfire2/issues/1347
    //    if (environment.production && "serviceWorker" in navigator)
    //      navigator.serviceWorker.getRegistration()
    //        .then(registration =>
    //          registration !== undefined ? registration : navigator.serviceWorker.register("./ngsw-worker.js"))
    //        .catch(e => { throw new Error(e); });

    ts.setDefaultLang("en");
    ts.addLangs(["de", "fr"]);
    ts.use(ts.getBrowserLang());

    for (const i of generateOneToNine()) {
      this.fieldCssClasses[i] = [];
      this.fieldStates[i] = [];
      this.grid[i] = [];
    }

    this.selectedPosition = this.ss.newGame();
    this.update();

    this.auth.authState.pipe(
      map(user => user !== null ? this.db.doc<UserData>(`users/${user.uid}`) : undefined),
      tap(userDoc => this.userDoc = userDoc),
      filter((userDoc): userDoc is AngularFirestoreDocument<UserData> => userDoc !== undefined),
      switchMap(userDoc => userDoc.valueChanges()))
      .subscribe(userData => {
        if (userData !== undefined) {
          if (userData.theme !== undefined)
            this.setThemeInternal(userData.theme);
          if (userData.gameData !== undefined) {
            ss.setGameData(userData.gameData);
            this.update(this.lastSolvedField);
          }
        }
      });
  }

  setTheme(theme: string): void {
    this.dismissAndClose();
    this.setThemeInternal(theme);
    this.saveUserData();
  }

  private setThemeInternal(theme: string): void {
    if (theme === "light")
      // tslint:disable-next-line:no-parameter-reassignment
      theme = "";
    if (this.currentTheme.length > 0) {
      this.renderer.removeClass(this.element.nativeElement, this.currentTheme);
      this.overlayContainer
        .getContainerElement()
        .classList
        .remove(this.currentTheme);
    }

    if (theme.length > 0) {
      this.renderer.addClass(this.element.nativeElement, theme);
      this.overlayContainer.
        getContainerElement()
        .classList
        .add(theme);
    }
    this.currentTheme = theme;
  }

  login(): void {
    this.dismissAndClose();
    this.dialog.open(LoginComponent);
  }

  logout(): void {
    this.dismissAndClose();
    void this.auth.auth.signOut();
  }

  about(): void {
    this.dismissAndClose();
    window.open("https://gitlab.com/winni/angular-sudoku#angular-sudoku");
  }

  get grid(): OneToNineOrUndefined[][] {
    return this.ss.grid;
  }

  get level(): Level {
    return this.ss.getLevel();
  }

  get score(): number {
    return this.ss.score;
  }

  get isUserDefined(): boolean {
    return this.ss.isUserDefined();
  }

  get pointsToWin(): number {
    return this.ss.pointsToWin;
  }

  // tslint:disable-next-line:prefer-function-over-method
  toggleTimer(): void {
    // TODO
  }

  // tslint:disable-next-line:prefer-function-over-method
  showStatistics(): void {
    // TODO
  }

  newGame(): void {
    this.dismissAndClose();
    confirm(!this.ss.isStarted() || this.ss.isSolved(),
      this.dialog, this.ts.instant("newGame"), this.ts.instant("confirmNewGame"), () => {
        this.selectedPosition = this.ss.newGame();
        this.updateAndSave();
      });
  }

  ownGame(): void {
    this.dismissAndClose();
    confirm(!this.ss.isStarted() || this.ss.isSolved(),
      this.dialog, this.ts.instant("ownGame"), this.ts.instant("confirmOwnGame"),
      () => {
        this.ss.clearGame();
        this.updateAndSave();
      });
  }

  solveNext(): void {
    this.snackBar.dismiss();
    const solvedField = this.ss.solveNextAndUpdate();
    if (solvedField === undefined)
      this.openSnackBar("warning", "notYetSolvable");
    this.updateAndSave(solvedField);
  }

  solveAll(): void {
    this.dismissAndClose();
    confirm(!this.ss.isStarted(),
      this.dialog, this.ts.instant("solveAll"), this.ts.instant("confirmSolveAll"),
      () => {
        this.ss.solveAllAndUpdate();
        if (!this.ss.isSolved())
          this.openSnackBar("warning", "notYetSolvable");
        this.updateAndSave();
      });
  }

  isSolved(): boolean {
    return this.ss.isSolved();
  }

  keydown(keyboardEvent: KeyboardEvent): void {
    this.dismissAndClose();
    // Do not handle and do not prevent default, if a dialog is open.
    if (this.dialog.openDialogs.length > 0)
      return;
    this.selectedPositionVisible = true;
    this.handleKeyboardEvent(keyboardEvent);
  }

  fieldClicked(row: OneToNine, col: OneToNine): void {
    this.dismissAndClose();
    this.selectedPositionVisible = false;
    this.selectedPosition = new Position(row, col);
    this.setSelectedDigitInSelectedField();
    this.updateAndSave();
  }

  digitClicked(value: OneToNine | 0): void {
    this.dismissAndClose();
    // 0 is used here instead of undefined, because nuundefinedll can not be an index of the digitCssClasses array
    this.selectedDigit = value === 0 ? undefined : value;
    this.updateAndSave();
  }

  // tslint:disable-next-line:cognitive-complexity
  private handleKeyboardEvent(keyboardEvent: KeyboardEvent): void {
    const key = keyboardEvent.key;
    if (key >= "1" && key <= "9")
      this.setSelectedDigitOrSetDigitInSelectedField(+key as OneToNine);
    else switch (key) {
      case " ":
        this.setSelectedDigitOrSetDigitInSelectedField();
        break;
      case "ArrowUp":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.prevRowBox();
        else
          this.selectedPosition.prevRow();
        break;
      case "ArrowDown":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.nextRowBox();
        else
          this.selectedPosition.nextRow();
        break;
      case "ArrowLeft":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.prevColBox();
        else
          this.selectedPosition.prevCol();
        break;
      case "ArrowRight":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.nextColBox();
        else
          this.selectedPosition.nextCol();
        break;
      case "Home":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.start();
        else
          this.selectedPosition.col = 1;
        break;
      case "End":
        if (keyboardEvent.ctrlKey)
          this.selectedPosition.end();
        else
          this.selectedPosition.col = 9;
        break;
      case "Tab":
        if (keyboardEvent.shiftKey)
          this.selectedPosition.prev();
        else
          this.selectedPosition.next();
        break;
      case "Enter":
        this.setSelectedDigitInSelectedField();
        break;
      case "Delete":
      case "Backspace":
        this.setDigitInSelectedField();
        break;
      default:
        return; // Do not prevent default for an unhandled key.
    }
    this.updateAndSave();
    keyboardEvent.preventDefault();
  }

  private setSelectedDigitOrSetDigitInSelectedField(digit?: OneToNine): void {
    if (digit === this.selectedDigit)
      this.setSelectedDigitInSelectedField();
    else
      this.selectedDigit = digit;
  }

  private setSelectedDigitInSelectedField(): void {
    if (this.ss.isInitialClue(this.selectedPosition))
      return;
    this.setDigitInSelectedField(this.selectedDigit);
  }

  private setDigitInSelectedField(digit?: OneToNine): void {
    const result = this.ss.setDigitChecked(this.selectedPosition, digit);
    if (result !== undefined) {
      if (result.type !== undefined && result.message !== undefined)
        this.openSnackBar(result.type, result.message, { digit });
      this.fieldStates = result.fieldStates;
    }
  }

  private updateAndSave(solvedField?: SolvedField): void {
    this.update(solvedField);
    this.saveUserData();
  }

  private update(solvedField?: SolvedField): void {
    this.lastSolvedField = solvedField;
    if (solvedField !== undefined) {
      this.selectedPosition = solvedField.position;
      this.selectedDigit = solvedField.digit;
      this.reason = this.ts.instant(solvedField.reason, solvedField);
    } else
      this.reason = "";
    this.updateFieldCssClasses(solvedField);
    this.updateDigitCssClasses();
  }

  private updateFieldCssClasses(solvedField?: SolvedField): void {
    const gridForOnlyOnePossibleDigit: boolean[][] = this.ss.getGridForOnlyOnePossibleDigit();
    for (const pos of allPositions())
      this.fieldCssClasses[pos.row][pos.col] = {
        groupForLastSolvedField: false,
        initialClue: this.ss.isInitialClue(pos),
        lastSolvedField: false,
        onlyOnePossibleDigit: gridForOnlyOnePossibleDigit[pos.row][pos.col],
        selectedDigit: this.ss.getDigit(pos) === this.selectedDigit && this.selectedDigit !== undefined,
        selectedPosition: false,
      };
    if (solvedField !== undefined) {
      this.fieldCssClasses[solvedField.position.row][solvedField.position.col].lastSolvedField = true;
      for (const p of solvedField.allPositionsForReason())
        this.fieldCssClasses[p.row][p.col].groupForLastSolvedField = true;
    }
    this.fieldCssClasses[this.selectedPosition.row][this.selectedPosition.col].selectedPosition =
      this.selectedPositionVisible;
  }

  private updateDigitCssClasses(): void {
    const exhaustedDigits = this.ss.getExhaustedDigits();
    for (const digit of generateOneToNine())
      this.digitCssClasses[digit] = {
        exhaustedDigit: exhaustedDigits[digit],
        selectedDigit: this.selectedDigit === digit,
      };
    this.digitCssClasses[0] = {
      exhaustedDigit: false,
      selectedDigit: this.selectedDigit === undefined,
    };
  }

  private openSnackBar(cssClass: string, message: string, params?: object): void {
    const config = new MatSnackBarConfig();
    config.verticalPosition = "bottom";
    config.horizontalPosition = "center";
    config.duration = 3000;
    config.panelClass = [cssClass];
    this.snackBar.open(this.ts.instant(message, params), undefined, config);
  }

  private dismissAndClose(): void {
    this.snackBar.dismiss();
    void this.sidenav.close();
  }

  private saveUserData(): void {
    if (this.userDoc !== undefined)
      void this.userDoc.set({
        gameData: this.ss.getGameData(),
        theme: this.currentTheme,
      });
  }
}

export function provideAppComponentAs(injectionToken: object): Provider {
  return { provide: injectionToken, useExisting: forwardRef(() => AppComponent) };
}
