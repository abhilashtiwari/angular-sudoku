import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { ErrorStateMatcher, MatButtonModule, MatDialogModule, MatGridListModule, MatIconModule, MatInputModule, MatMenuModule, MatSidenavModule, MatSnackBarModule, MatTabsModule, MatToolbarModule, MatTooltipModule, ShowOnDirtyErrorStateMatcher } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AppComponent } from "app/app.component";
import { ConfirmComponent } from "app/confirm/confirm.component";
import { LoginComponent } from "app/login/login.component";
import { DigitComponent } from "./digit/digit.component";
import { GridComponent } from "./grid/grid.component";
import { SidenavComponent } from "./sidenav/sidenav.component";
import { ToolbarComponent } from "./toolbar/toolbar.component";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient): TranslateLoader {
  return new TranslateHttpLoader(http, "./assets/i18n/");
}

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    ConfirmComponent,
    LoginComponent,
    DigitComponent,
    GridComponent,
    SidenavComponent,
    ToolbarComponent,
  ],
  entryComponents: [
    ConfirmComponent,
    LoginComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTabsModule,
    MatInputModule,
    MatTooltipModule,
    MatMenuModule,
    MatGridListModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        deps: [HttpClient],
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
      },
    }),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDLtCpl-B0yD4_Nr-ulcokswM9PKnK05IM",
      authDomain: "angular-sudoku.firebaseapp.com",
      databaseURL: "https://angular-sudoku.firebaseio.com",
      messagingSenderId: "995445029311",
      projectId: "angular-sudoku",
      storageBucket: "angular-sudoku.appspot.com",
    }),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    //    ServiceWorkerModule.register("/ngsw-worker.js",
    //      { enabled: environment.production }),
  ],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }],
})
export class AppModule { }
